/**
 * 
 */
package org.cuatrovientos.hibernate;

import java.util.List;

/**
 * @author David
 *
 */
public interface SellerDAOInterface {

	public List<Seller> showAll ();
	public Seller selectId (long id);
	public void delete(Seller seller);
	public void insert(Seller seller);
	public void update(Seller seller);
}
