/**
 * 
 */
package org.cuatrovientos.hibernate;

/**
 * Represent a seller
 * @author David
 *
 */
public class Seller {

	private int id;
	private String name;
	private float percentage;
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the percentage
	 */
	public float getPercentage() {
		return percentage;
	}
	/**
	 * @param percentage the percentage to set
	 */
	public void setPercentage(float percentage) {
		this.percentage = percentage;
	}
	/**
	 * @param id
	 * @param name
	 * @param percentage
	 */
	public Seller(int id, String name, float percentage) {
		super();
		this.id = id;
		this.name = name;
		this.percentage = percentage;
	}
	
	public Seller() {
		super();
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Seller [id=" + id + ", name=" + name + ", percentage="
				+ percentage + "]";
	}
	
	
}
