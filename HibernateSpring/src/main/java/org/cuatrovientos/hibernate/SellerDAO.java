/**
 * 
 */
package org.cuatrovientos.hibernate;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 * @author David
 *
 */
public class SellerDAO implements SellerDAOInterface {

	public List<Seller> showAll() {
		SessionFactory sessionFactory = HibernateSession.getSessionFactory();
		Session session = sessionFactory.openSession();
		
		@SuppressWarnings("unchecked")
		List<Seller> sellers = (List<Seller>) session.createQuery("from seller").list();
		session.close();
		return sellers;
	}

	public Seller selectId(long id) {
		SessionFactory sessionFactory = HibernateSession.getSessionFactory();
		Session session = sessionFactory.openSession();
		
		Seller seller = (Seller)(session.get(Seller.class,id));
		session.close();
		return seller;
		
	}

	public void delete(Seller seller) {
		SessionFactory sessionFactory = HibernateSession.getSessionFactory();
		Session session = sessionFactory.openSession();
		
		session.beginTransaction();
		session.delete(seller);
		session.getTransaction().commit();
		session.close();
	}

	public void insert(Seller seller) {
		SessionFactory sessionFactory = HibernateSession.getSessionFactory();
		Session session = sessionFactory.openSession();
		
		session.beginTransaction();
		int id = (Integer) session.save(seller);
		seller.setId(id);
		session.getTransaction().commit();
		session.close();
		
	}

	public void update(Seller seller) {
		SessionFactory sessionFactory = HibernateSession.getSessionFactory();
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		session.merge(seller);
		session.getTransaction().commit();
		
		session.close();
		
	}

}
